package com.example.pawe.currencyconventer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editTextInput = (EditText) findViewById(R.id.editTextInput);
        Button buttonToUsd = (Button) findViewById(R.id.buttonToUsd);
        Button buttonToPln = (Button) findViewById(R.id.buttonToPln);
        final TextView textViewResult = (TextView) findViewById(R.id.textViewResult);

//        TextView textViewCurrentUSD = (TextView) findViewById(R.id.TextViewCurrentUSD);
        final CurrentCourse currentValueOfUSD = new CurrentCourse();
//        textViewCurrentUSD.setText(currentValueOfUSD.toString());


        buttonToPln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Click!", Toast.LENGTH_SHORT).show();
                float input = Float.parseFloat(editTextInput.getText().toString());
                input *= currentValueOfUSD.currentUSD();
                textViewResult.setText("Result: " + input);
            }
        });
        buttonToUsd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Click!", Toast.LENGTH_SHORT).show();
                float input = Float.parseFloat(editTextInput.getText().toString());
                input /= currentValueOfUSD.currentUSD();
                textViewResult.setText("Result: " + input);
            }
        });
    }

//    public static void main(String[] args) {
//        CurrentCourse currentCourse = new CurrentCourse();
////        String x = currentCourse.toString();
////        double y = Double.parseDouble(x);
////        System.out.println(y);
//        System.out.println(currentCourse);
//    }
}
