package com.example.pawe.currencyconventer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class CurrentCourse {

    private Float currentUSD;

    //zkopipastowana klasa i przerobiona na uzytek aplikacji
    public CurrentCourse() {}

    public Float currentUSD(){
        try {

            URL url = new URL("http://www.nbp.pl/kursy/xml/LastC.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(url.openStream());
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("pozycja");

            for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);
                Element element = (Element) node;
// pobieram kod waluty, sprawdzam czy to USD, jeżeli tak pobieram z niego wartosc(kurs kupna)
                NodeList currencyCode = element.getElementsByTagName("kod_waluty");
                Element codeElement = (Element) currencyCode.item(0);
                currencyCode = codeElement.getChildNodes();
                String nameUSD = currencyCode.item(0).getNodeValue();

                if (nameUSD.compareTo("USD") == 0) {
                    NodeList buyRate = element.getElementsByTagName("kurs_kupna");
                    Element buyRateElement = (Element) buyRate.item(0);
                    buyRate = buyRateElement.getChildNodes();
                    String buyRateString = buyRate.item(0).getNodeValue();
                    buyRateString = buyRateString.replaceAll(",", ".");
                    currentUSD = Float.parseFloat(buyRateString);
                    return currentUSD;
                }
            }
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
        return currentUSD;
    }

    @Override
    public String toString() {
        return "" + currentUSD();
    }
}
